package com.rt.callrec;

/**
 * Created by mac24h on 6/8/18.
 */

public class Facade {
    public interface Shape {
        String draw();
        int getSize();
    }

    public class Rectangle implements Shape{

        @Override
        public String draw() {
            return "This is Rectangle";
        }

        @Override
        public int getSize() {
            return 10;
        }
    }

    public class Circle implements Shape{

        @Override
        public String draw() {
            return "This is Circle";
        }

        @Override
        public int getSize() {
            return 5;
        }
    }
}
